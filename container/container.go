package main

import "fmt"

type count int

func main() {
	list := make([]int, 10, 20)
	list = []int{1, 2, 3, 4}
	list = append(list, 1)
	list = list[1:3]
	list = list[1:]
	fmt.Println(list)

	table := make(map[string]int)
	table["one"] = 1
	fmt.Println(table["one"])
	delete(table, "one")
}
