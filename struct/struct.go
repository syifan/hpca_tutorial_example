package main

type Rectangle struct {
	Width  float64
	Height float64
	x, y   float64
}

func (r Rectangle) Size() float64 {
	return r.Width * r.Height
}

func (r *Rectangle) SetY(y float64) {
	r.y = y
}

func NewRectangle() *Rectangle {
	r := Rectangle{
		Width: 10,
		x:     3,
	}
	return &r
}

func NewRectangle2() *Rectangle {
	r := new(Rectangle)
	r.Width = 10
	r.x = 3
	return r
}
