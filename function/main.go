package main

import "sync"

var lock sync.Mutex

func deferFunction() {
	lock.Lock()
	defer lock.Unlock()

}

func Print1() {
}

func Print2(word string) {

}

func Print3(x, y int, word string) {

}

func Print4(x, y int, word string) int {
	return 1
}

func Print5(x, y int, word string) (numCharPrinted int) {
	return 1
}

func Print6(x, y int, word string) (numCharPrinted int, err error) {
	return 1, nil
}

func Div(numerator, denominator int) (quotient, remainer int) {
	return numerator / denominator, numerator % denominator
}
