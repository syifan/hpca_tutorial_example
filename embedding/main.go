package main

import "fmt"

type Writer interface {
	Write(content string) (err error)
}

type Console struct{}

func (c Console) Write(content string) error {
	fmt.Print(content)
	return nil
}

type Logger struct {
	Console
	Prefix string
}

func (l Logger) Log(content string) {
	l.Console.Write(l.Prefix + content)
}

func main() {
	l := Logger{
		Console: Console{},
		Prefix:  "Log: ",
	}

	l.Write("Hello World!\n")
	l.Log("Hello World!\n")
}
