package main

import "fmt"

func main() {
	i := 10
	if i > 5 {
		fmt.Printf("i > 5\n")
	} else {
		fmt.Printf("i <= 5\n")
	}
}
