package main

import (
	"fmt"

	"gitlab.com/akita/akita"
)

func main() {
	freq := 1.8 * akita.GHz
	fmt.Printf("freq is %f\n", freq)
}
