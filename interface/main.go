package main

import "fmt"

type Writer interface {
	Write(content string) (err error)
}

type Console struct{}

func (c Console) Write(content string) error {
	fmt.Print(content)
	return nil
}

func Log(w Writer, content string) {
	err := w.Write(content)
	if err != nil {
		panic(err)
	}
}

func main() {
	c := Console{}
	Log(c, "Hello World!\n")
}
