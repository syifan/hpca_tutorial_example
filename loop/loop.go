package main

import "fmt"

func main() {
	for {
		break
	}

	i := 10
	for i > 0 {
		i--
	}

	array := make([]int, 16)
	for index, value := range array {
		fmt.Printf("index: %d, value %d\n", index, value)
	}

	for i := 0; i < 10; i++ {

	}
}
